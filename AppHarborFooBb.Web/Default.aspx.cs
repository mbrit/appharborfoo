﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppHarborFooBb.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public string Message
        {
            get
            {
                return "Welcome to AppHarbor via Bitbucket - " + DateTime.Now.ToString(); // commit
            }
        }
    }
}

